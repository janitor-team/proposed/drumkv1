Source: drumkv1
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Dennis Braun <d_braun@kabelmail.de>,
 Jaromír Mikeš <mira.mikes@seznam.cz>,
 IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 ladspa-sdk,
 libasound2-dev,
 libjack-dev | libjack-jackd2-dev,
 qtbase5-dev,
 qttools5-dev-tools,
 libsndfile1-dev,
 libtool,
 libx11-dev,
 lv2-dev,
 liblo-dev,
Rules-Requires-Root: no
Standards-Version: 4.6.0
Homepage: https://drumkv1.sourceforge.io/
Vcs-Git: https://salsa.debian.org/multimedia-team/drumkv1.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/drumkv1

Package: drumkv1
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 drumkv1-common (= ${source:Version}),
Recommends:
 jackd
Description: old-school drum-kit sampler - standalone
 drumkv1 is an old-school all-digital drum-kit sampler
 synthesizer with stereo effects. It is provided in both
 forms of a LV2 plugin and a pure stand-alone JACK
 client with JACK-session and both JACK MIDI and ALSA
 MIDI input support.
 .
 This package provides the standalone app.

Package: drumkv1-lv2
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 drumkv1-common (= ${source:Version}),
Provides:
 lv2-plugin
Description: old-school drum-kit sampler - lv2-plugin
 drumkv1 is an old-school all-digital drum-kit sampler
 synthesizer with stereo effects. It is provided in both
 forms of a LV2 plugin and a pure stand-alone JACK
 client with JACK-session and both JACK MIDI and ALSA
 MIDI input support.
 .
 This package provides the LV2 plugin.

Package: drumkv1-common
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: old-school drum-kit sampler - common files
 drumkv1 is an old-school all-digital drum-kit sampler
 synthesizer with stereo effects. It is provided in both
 forms of a LV2 plugin and a pure stand-alone JACK
 client with JACK-session and both JACK MIDI and ALSA
 MIDI input support.
 .
 This package provides files shared by both the LV2 plugin and the standalone
 application.
